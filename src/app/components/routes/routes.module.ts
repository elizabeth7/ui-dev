import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { ServicioComponent } from './servicio/servicio.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { ContactoComponent } from './contacto/contacto.component';
import { MatTabsModule } from '@angular/material/tabs';

import { MatIconModule } from '@angular/material/icon';
import { NotfoundComponent } from './notfound/notfound.component';
import { AboutUsComponent } from './about-us/about-us.component';


@NgModule({
  declarations: [
    InicioComponent,
    ServicioComponent,
    GaleriaComponent,
    ContactoComponent,
    NotfoundComponent,
    AboutUsComponent
  ],
  imports: [
    CommonModule,
    MatTabsModule,
    MatIconModule
  ]
})
export class RoutesModule { }
